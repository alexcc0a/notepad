package com.nesterov;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Scanner scannerInput = new Scanner(System.in); // Создается объект scannerInput класса Scanner для считывания ввода с клавиатуры.
        List<String> list = new ArrayList<String>(); // Создается список list, который будет хранить задачи. Он реализован как ArrayList с элементами типа String.

        while (true) { // Начало бесконечного цикла, который позволяет пользователю взаимодействовать с программой до тех пор, пока он не выберет выход.
            System.out.println(" Выберите действие:");
            System.out.println("1. Добавить задачу");
            System.out.println("2. Вывести список задач");
            System.out.println("3. Удалить задачу");
            System.out.println("0. Выход");
            int input = scannerInput.nextInt(); // Вводится выбор пользователя в переменную input.
            int count = 1; // Инициализируется переменная count, которая будет использоваться для нумерации задач.

            switch (input) { // Начало конструкции switch, которая обрабатывает выбор пользователя.
                // Остальные строчки кода внутри switch обрабатывают различные действия в зависимости от выбора пользователя.
                case 1:
                    System.out.println("Введите задачу для планирования (для завершения введите end!)");
                    Scanner scannerTask = new Scanner(System.in);
                    String task = scannerTask.nextLine(); // Считывание текста задачи с клавиатуры.

                    if ("end".equals(input)) { // Проверка, если пользователь ввел "end", то...
                        for (int i = 0; i < list.size(); i++) {
                            System.out.println(list.get(i) + "\n"); // Вывод всех задач из списка.
                        }
                        return; // Завершение программы.
                    }
                    list.add(task); // Добавление задачи в список.
                    count++; // Увеличение счетчика задач.
                    break;

                case 2:
                    // Вывод списка задач.
                    System.out.println("\n");
                    System.out.println("Список задач:");
                    System.out.println("\n");

                    for (int i = 0; i < list.size(); i++) {
                        System.out.println((count++) + ". " + list.get(i) +"\n"); // Вывод нумерованного списка задач.
                    } break;

                case 3:
                    // Удаление задачи из списка.
                    System.out.println("Введите номер задачи которую вы хотите удалить!");
                    Scanner scannerTaskInt = new Scanner(System.in);
                    int taskInt = scannerTaskInt.nextInt()-1; // Считывание номера задачи для удаления.
                    list.remove(taskInt); // Удаление задачи по указанному номеру
                    break;

                case 0:
                    // Выход из программы.
                    return;

                default:
                    // Обработка некорректного выбора.
                    System.out.println("Неверно указано значение"); // Вывод сообщения об ошибке.
                    break;
            }
        }
    }
}